import { ParallaxBanner } from "react-scroll-parallax";
import { HeroProps } from "./interface";

export default function Hero({ judul }: HeroProps) {
  return (
    <ParallaxBanner
      className="overflow-hidden w-screen clip-triangle"
      layers={[
        {
          image: "/assets/images/background.jpg",
          speed: -35,
        },
      ]}
      style={{ height: "55vh" }}
    >
      <div className="relative w-screen z-10 h-full flex flex-col items-center justify-center text-white">
        <h1 className="text-5xl">{judul}</h1>
        <p className="text-xl">Where all our great things begin</p>
      </div>
    </ParallaxBanner>
  );
}
