import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { NavLinkProps } from "./interface";

export default function Navbar() {
  const router = useRouter();
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [visible, setVisible] = useState(true);
  const [opacity, setOpacity] = useState(1);

  const isActive = (href: string) =>
    router.pathname === href ? "border-b-4 p-2 border-white" : "";

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      setVisible(prevScrollPos > currentScrollPos || currentScrollPos < 10);
      setOpacity(
        currentScrollPos === 0
          ? 1
          : currentScrollPos < prevScrollPos
          ? 0.8
          : opacity
      );
      setPrevScrollPos(currentScrollPos);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [prevScrollPos, opacity]);

  const navbarStyle = {
    top: visible ? "0" : "-100px",
    transition: "top 0.3s",
    opacity: opacity,
  };

  return (
    <nav
      className="bg-[#ff6600] w-screen h-[75px] flex flex-row items-center justify-between px-24 fixed z-20"
      style={navbarStyle}
    >
      <Image
        src="/assets/images/logosuitmedia.png"
        alt="logo"
        width={120}
        height={45}
      />
      <NavLinks isActive={isActive} />
    </nav>
  );
}

const NavLinks = ({ isActive }: { isActive: (href: string) => string }) => {
  const links = [
    { href: "/work", label: "Work" },
    { href: "/about", label: "About" },
    { href: "/services", label: "Services" },
    { href: "/", label: "Ideas" },
    { href: "/careers", label: "Careers" },
    { href: "/contact", label: "Contact" },
  ];

  return (
    <div className="text-white text-md flex flex-row items-center justify-evenly gap-5">
      {links.map((link) => (
        <NavLink
          key={link.href}
          href={link.href}
          activeStyle={isActive(link.href)}
        >
          {link.label}
        </NavLink>
      ))}
    </div>
  );
};

const NavLink = ({ href, children, activeStyle }: NavLinkProps) => (
  <Link href={href}>
    <div className={`cursor-pointer ${activeStyle}`}>{children}</div>
  </Link>
);
