export type NavLinkProps = {
    href: string;
    children: React.ReactNode;
    activeStyle: string;
  };