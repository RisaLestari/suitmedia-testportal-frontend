export type Idea = {
    id: number;
    published_at: string;
    title: string;
    small_image?: { url: string }[];
    medium_image?: { url: string }[];
  };
  
export type Props = {
    ideas?: Idea[];
  };