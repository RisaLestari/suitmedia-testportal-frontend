import Image from "next/image";
import { Idea, Props } from "./interface";

const dateConverter = (dateString: string): string => {
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const date = new Date(dateString);
  return new Intl.DateTimeFormat("id-ID", options).format(date);
};

const Card: React.FC<Props> = ({ ideas }) => (
  <div className="flex flex-row gap-5 flex-wrap justify-center mt-6">
    {ideas?.map((idea) => (
      <div key={idea.id} className="w-[280px] h-[300px] shadow-md rounded-xl">
        <div className="w-full h-[60%] relative">
          <Image
            src={
              idea.small_image?.[0]?.url ||
              idea.medium_image?.[0]?.url ||
              "/assets/images/background.jpg"
            }
            alt="image"
            layout="fill"
            className="rounded-t-xl bg-cover"
            loading="lazy"
          />
        </div>
        <div className="w-full p-5">
          <p className="text-gray-400">{dateConverter(idea.published_at)}</p>
          <h1 className="text-md font-bold overflow-hidden line-clamp-3">
            {idea.title}
          </h1>
        </div>
      </div>
    ))}
  </div>
);

export default Card;
