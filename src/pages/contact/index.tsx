import Navbar from "@/src/components/Elements/Navbar";
import Hero from "@/src/components/Modules/Hero";
import { ParallaxProvider } from "react-scroll-parallax";

export default function Contact() {
  return (
    <div>
      <Navbar />

      <ParallaxProvider>
        <Hero judul="Contact" />
      </ParallaxProvider>

      <div className="my-96 text-transparent">a</div>
      <div className="my-96 text-transparent">a</div>
    </div>
  );
}
