import Navbar from "@/src/components/Elements/Navbar";
import Hero from "@/src/components/Modules/Hero";
import { ParallaxProvider } from "react-scroll-parallax";

export default function About() {
  return (
    <div>
      <Navbar />

      <ParallaxProvider>
        <Hero judul="About" />
      </ParallaxProvider>

      <div className="my-96 text-transparent">a</div>
      <div className="my-96 text-transparent">a</div>
    </div>
  );
}
