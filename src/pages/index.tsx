import { GetServerSideProps } from "next";
import { useState, useEffect } from "react";
import axios from "axios";
import { ParallaxProvider } from "react-scroll-parallax";
import Image from "next/image";
import { useRouter } from "next/router";

import Card from "@/src/components/Elements/Card";
import Navbar from "@/src/components/Elements/Navbar";
import Hero from "@/src/components/Modules/Hero";

const setLocalStorageItem = (key: string, value: any) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(key, JSON.stringify(value));
  }
};

const getLocalStorageItem = (key: string, defaultValue: any) => {
  if (typeof window !== "undefined") {
    const storedItem = localStorage.getItem(key);
    if (storedItem) {
      try {
        return JSON.parse(storedItem);
      } catch (error) {
        console.error(`Error parsing JSON for key ${key}:`, error);
      }
    }
  }
  return defaultValue;
};

const fetchIdeas = async (
  currentPage: number,
  perPage: number,
  sortBy: string
) => {
  const response = await axios.get(
    `https://suitmedia-backend.suitdev.com/api/ideas`,
    {
      params: {
        "page[number]": currentPage,
        "page[size]": perPage,
        append: ["small_image", "medium_image"],
        sort: sortBy,
      },
    }
  );
  return response.data;
};

export default function MainPage({ initialIdeas, startItemKey }: any) {
  const router = useRouter();
  const { query } = router;

  const storedPerPage = getLocalStorageItem("perPage", 10);
  const storedSortBy = getLocalStorageItem("sortBy", "-published_at");
  const storedCurrentPage = getLocalStorageItem("currentPage", 1);
  const storedActivePage = getLocalStorageItem("activePage", 1);

  const [perPage, setPerPage] = useState<number>(
    query.perPage ? Number(query.perPage) : storedPerPage
  );
  const [sortBy, setSortBy] = useState<"published_at" | "-published_at">(
    query.sortBy
      ? (query.sortBy as "published_at" | "-published_at")
      : storedSortBy
  );
  const [currentPage, setCurrentPage] = useState<number>(
    query.pageNumber ? Number(query.pageNumber) : storedCurrentPage
  );
  const [data, setData] = useState(initialIdeas);
  const [activePage, setActivePage] = useState<number>(
    storedActivePage || currentPage
  );
  const [startItem, setStartItem] = useState<string>(startItemKey);

  useEffect(() => {
    setLocalStorageItem("perPage", perPage);
  }, [perPage]);

  useEffect(() => {
    setLocalStorageItem("sortBy", sortBy);
  }, [sortBy]);

  useEffect(() => {
    setLocalStorageItem("currentPage", currentPage);
  }, [currentPage]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await fetchIdeas(currentPage, perPage, sortBy);
        setData(data);
        if (!startItem && data?.data?.length > 0) {
          setStartItem(data.data[0].id);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    const totalPages = Math.ceil(initialIdeas.meta.total / perPage);
    if (currentPage > totalPages) {
      setCurrentPage(totalPages);
    } else {
      fetchData();
    }
  }, [perPage, sortBy, currentPage, startItem]);

  useEffect(() => {
    router.push({
      pathname: router.pathname,
      query: { perPage, sortBy, pageNumber: currentPage },
    });
    setLocalStorageItem("startItemKey", startItem);
  }, [perPage, sortBy, currentPage, startItem]);

  const handlePerPageChange = (perPageValue: number) => {
    setPerPage(perPageValue);
  };

  const handleSortChange = (sortValue: "published_at" | "-published_at") => {
    setSortBy(sortValue);
  };

  const handlePageChange = (pageNumber: number) => {
    setCurrentPage(pageNumber);
    setActivePage(pageNumber);
    setLocalStorageItem("activePage", pageNumber);
  };

  const generatePages = () => {
    const totalPages = Math.ceil(initialIdeas.meta.total / perPage);
    const maxPages = 5;
    const startPage = Math.max(currentPage - Math.floor(maxPages / 2), 1);
    const endPage = Math.min(startPage + maxPages - 1, totalPages);

    return {
      pages: Array.from(
        { length: endPage - startPage + 1 },
        (_, i) => startPage + i
      ),
      activePage: currentPage,
    };
  };

  return (
    <main>
      <Navbar />
      <ParallaxProvider>
        <Hero judul="Ideas" />
      </ParallaxProvider>

      <div className="mx-36 w-10/12 mt-14">
        <div className="flex flex-row items-center justify-between">
          <p className="font-medium ml-3">
            Showing {currentPage === 1 ? 1 : (currentPage - 1) * perPage + 1} -{" "}
            {Math.min(currentPage * perPage, initialIdeas.meta.total)} of{" "}
            {initialIdeas.meta.total}
          </p>

          <div className="flex flex-row items-center gap-x-6 font-medium">
            <div className="flex flex-row item-center gap-x-2">
              <p>Show per page: </p>
              <select
                onChange={(e) => handlePerPageChange(Number(e.target.value))}
                className="border-2 w-24 rounded-lg"
                value={perPage}
              >
                <option value={10}>10</option>
                <option value={20}>20</option>
                <option value={50}>50</option>
              </select>
            </div>

            <div className="flex flex-row item-center gap-x-2">
              <p>Sort by: </p>
              <select
                onChange={(e) =>
                  handleSortChange(
                    e.target.value as "published_at" | "-published_at"
                  )
                }
                className="border-2 w-24 rounded-lg"
                value={sortBy}
              >
                <option value="-published_at">Newest</option>
                <option value="published_at">Oldest</option>
              </select>
            </div>
          </div>
        </div>

        <Card ideas={data.data} />

        <div className="flex items-center justify-center mt-8">
          <button
            onClick={() => handlePageChange(1)}
            disabled={currentPage === 1}
            className="mx-1 px-3 py-1"
          >
            <Image
              src="/assets/icons/double_arrow.svg"
              alt="next"
              width={30}
              height={30}
            />
          </button>

          <button
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
            className="mx-1 px-3 py-1 transform rotate-180"
          >
            <Image
              src="/assets/icons/single_arrow.svg"
              alt="previous"
              width={30}
              height={30}
            />
          </button>

          {generatePages().pages.map((page) => (
            <button
              key={page}
              onClick={() => handlePageChange(page)}
              className={`mx-1 px-3 py-1 rounded ${
                generatePages().activePage === page
                  ? "bg-[#ff6600] text-white"
                  : ""
              }`}
            >
              {page}
            </button>
          ))}

          <button
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={
              currentPage === Math.ceil(initialIdeas.meta.total / perPage)
            }
            className="mx-1 px-3 py-1"
          >
            <Image
              src="/assets/icons/single_arrow.svg"
              alt="next"
              width={30}
              height={30}
            />
          </button>

          <button
            onClick={() =>
              handlePageChange(Math.ceil(initialIdeas.meta.total / perPage))
            }
            disabled={
              currentPage === Math.ceil(initialIdeas.meta.total / perPage)
            }
            className="mx-1 px-3 py-1 transform rotate-180"
          >
            <Image
              src="/assets/icons/double_arrow.svg"
              alt="next"
              width={30}
              height={30}
            />
          </button>
        </div>
      </div>

      <div className="my-10 text-transparent">a</div>
    </main>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const res = await fetch(
      "https://suitmedia-backend.suitdev.com/api/ideas?page[number]=1&page[size]=10&append[]=small_image&append[]=medium_image&sort=-published_at",
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );

    if (!res.ok) {
      throw new Error("Failed to fetch");
    }

    const data = await res.json();

    return {
      props: {
        initialIdeas: data,
        startItemKey: data.data[0]?.id || null,
      },
    };
  } catch (error) {
    console.error("Error fetching data:", error);
    return {
      props: {
        initialIdeas: [],
        startItemKey: null,
      },
    };
  }
};
